package net.jsontoxmlbeans;

import java.lang.reflect.Method;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;

/*
 * This class is the main entry point for the OSB Java callouts.
 * 
 * It uses reflection to load argo.* JSON parser classes within its own loader. This way OSB method selection dialog cannot find
 * the argo.* public static methods, and the dialog is not getting cluttered with them.
 * 
 * The argo.* classes are within the same JAR, but renamed into *.bin. The InnerClassLoader class loads them as resources on demand
 * and defines them as classes.
 * 
 */
public class Convertor {

	private static volatile Method toXML;
	private static volatile Method toJSON;

	/** 
	 * Convert JSON from string to XmlObject.
	 * @param json JSON passed as string
	 * @return XmlObject
	 */
	public static XmlObject toXML(Object o) throws XmlException {
		try {
			InnerClassLoader.install();
			
			Method m = getToXML();
			return (XmlObject) m.invoke(null,new Object[]{o});
		} catch( Exception ex ) { 
			throw new XmlException(ex);
		} finally {
			InnerClassLoader.remove();
		}
	}

	/** 
	 * Convert XmlObject built in specific format to JSON.
	 * @param xml XML to convert
	 * @return JSON string
	 * @throws XmlException 
	 */
	public static String toJSON(XmlObject xml) throws XmlException {
		try {
			InnerClassLoader.install();

			Method m = getToJSON();
			return (String) m.invoke(null,new Object[]{xml});
		} catch( Exception ex ) { 
			throw new XmlException(ex);
		} finally {
			InnerClassLoader.remove();
		}
	}
	
	private static Method getToXML() throws Exception {
		if( toXML == null ) toXML = get("toXML",Object.class);
		return toXML;
	}

	private static Method getToJSON() throws Exception {
		if( toJSON == null ) toJSON = get("toJSON",XmlObject.class);
		return toJSON;
	}
	
	private static Method get(String name, Class ... types) throws Exception {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Class<?> clz = cl.loadClass("net.jsontoxmlbeans.inner.ConvertorInner");
		Method m = clz.getDeclaredMethod(name,types);
		return m;
	}
}
