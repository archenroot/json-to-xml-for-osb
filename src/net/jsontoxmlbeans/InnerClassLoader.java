package net.jsontoxmlbeans;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

class InnerClassLoader extends ClassLoader {
	private static volatile InnerClassLoader icl;
	
	public InnerClassLoader(ClassLoader parent) {
		super(parent);
	}

	static void install() {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		if( cl instanceof InnerClassLoader ) return;
		
		if( icl == null ) icl = new InnerClassLoader(cl);
		Thread.currentThread().setContextClassLoader(icl);
	}

	static void remove() {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		if( !(cl instanceof InnerClassLoader) ) return;
		
		Thread.currentThread().setContextClassLoader(cl.getParent());
	}

	@Override
	protected synchronized Class<?> loadClass(String name, boolean resolve)	throws ClassNotFoundException {
		Class found = findLoadedClass(name);
		if( found != null ) return found;
		
		if( name.startsWith("net.jsontoxmlbeans") || name.startsWith("argo.") ) {
			String path = name.replaceAll("\\.","/");
			
			if( name.startsWith("argo.") ) path += ".bin";
			else if( name.startsWith("net.jsontoxmlbeans.inner.") ) path += ".bin";
			else path += ".class";
			
			InputStream is = getResourceAsStream(path);
			if( is == null ) return super.loadClass(name, resolve);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			
			try {
				byte buf[] = new byte[15000];
				while(true) {
					int n = is.read(buf);
					if( n < 0 ) break;
					baos.write(buf, 0, n);
				}
				
				is.close();
				baos.close();
			} catch( Exception ex ) {
				throw new ClassNotFoundException("failed to load class "+name+" due to: "+ex.getMessage());
			}
			
			byte[] data = baos.toByteArray();
			
			Class<?> defined = defineClass(name, data, 0, data.length);
			return defined;
		}
		
		return super.loadClass(name, resolve);
	}

	/**
	 * This is to provide a message in OSB method selection dialog. :-)
	 */
	@SuppressWarnings("rawtypes")
	public static void DO_NOT_USE_METHODS_OF_THIS_CLASS_IN_OSB_JAVA_CALLOUTS____YOU_NEED_CONVERTOR_CLASS() {
	}
}
