package net.jsontoxmlbeans.inner;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;

import argo.saj.JsonListener;

class JsonToXmlBeansListener implements JsonListener {
	private XmlObject xml;
	private XmlCursor cur;
	
	public JsonToXmlBeansListener() throws XmlException {
		xml = XmlObject.Factory.parse("<json xmlns='"+Utils.OJTX_NS+"'/>");
		cur = xml.newCursor();
		
		cur.toChild(Utils.qname("json"));
		cur.toEndToken();
	}
	
	/** 
	 * Return built XmlBeans object
	 * @return
	 */
	public XmlObject getXmlObject() {
		cur.dispose();
		return xml;
	}
	
	@Override
	public void startDocument() {
	}

	@Override
	public void endDocument() {
	}

	@Override
	public void startArray() {
		cur.beginElement(Utils.qname("array"));
	}

	@Override
	public void endArray() {
		cur.toNextToken();
	}

	@Override
	public void startObject() {
		cur.beginElement(Utils.qname("object"));
	}

	@Override
	public void endObject() {
		cur.toNextToken();
	}

	@Override
	public void startField(String name) {
		cur.beginElement(Utils.qname("field"));
		cur.insertAttributeWithValue("name",name);
	}

	@Override
	public void endField() {
		cur.toNextToken();
	}

	@Override
	public void stringValue(String value) {
		cur.beginElement(Utils.qname("string"));
		cur.insertAttributeWithValue("value",value);
		cur.toNextToken();
	}

	@Override
	public void numberValue(String value) {
		cur.beginElement(Utils.qname("number"));
		cur.insertAttributeWithValue("value",value);
		cur.toNextToken();
	}

	@Override
	public void trueValue() {
		cur.beginElement(Utils.qname("boolean"));
		cur.insertAttributeWithValue("value","true");
		cur.toNextToken();
	}

	@Override
	public void falseValue() {
		cur.beginElement(Utils.qname("boolean"));
		cur.insertAttributeWithValue("value","false");
		cur.toNextToken();
	}

	@Override
	public void nullValue() {
		cur.beginElement(Utils.qname("null"));
		cur.toNextToken();
	}
}
