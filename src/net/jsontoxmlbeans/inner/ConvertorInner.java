package net.jsontoxmlbeans.inner;

import java.io.IOException;
import java.io.StringReader;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;

import argo.saj.InvalidSyntaxException;
import argo.saj.SajParser;

public class ConvertorInner {

	private static final SajParser jsonParser = new SajParser();
	
	/** 
	 * Convert JSON from string to XmlObject.
	 * @param json JSON passed as string
	 * @return XmlObject
	 */
	public static XmlObject toXML(Object o) throws XmlException {
        if( o == null ) return null;
        
        try {
	        if( o instanceof String ) {
	        	String json = (String)o;
		        return getJson(json);
	        } else if( o instanceof org.xmlsoap.schemas.soap.envelope.impl.BodyImpl ) {
	        	org.xmlsoap.schemas.soap.envelope.impl.BodyImpl bi = (org.xmlsoap.schemas.soap.envelope.impl.BodyImpl)o;
	        	return getJson(bi.getStringValue());
	        } else if( o instanceof org.apache.xmlbeans.XmlObject ) {
	        	XmlObject xo = (XmlObject) o;
	        	XmlCursor cur = xo.newCursor();
	        	cur.toChild(0);
	        	XmlObject ret = getJson(cur.getTextValue());
	        	cur.dispose();
	        	return ret;
	        } else {
	        	throw new XmlException("only can parse JSON passed as string ($body/*[1]) or BodyImpl ($body) in "+o.getClass());
	        }
        } catch( XmlException xex ) {
        	throw xex;
        } catch( Exception ex ) {
        	throw new XmlException(ex);
        }
	}

	private static XmlObject getJson(String json) throws XmlException,IOException, InvalidSyntaxException {
		if( json.trim().length() == 0 ) return null;
		
		JsonToXmlBeansListener listener = new JsonToXmlBeansListener();
		
		StringReader sr = new StringReader(json.trim());
		jsonParser.parse(sr, listener);
		sr.close();
   
		return listener.getXmlObject();
	}
	
	/** 
	 * Convert XmlObject built in specific format to JSON.
	 * @param xml XML to convert
	 * @return JSON string
	 * @throws XmlException 
	 */
	public static String toJSON(XmlObject xml) throws XmlException {
        if( xml == null ) return null;

        XmlBeansToJsonConvertor convertor = new XmlBeansToJsonConvertor(xml);
        return convertor.getJSON();
	}
}
