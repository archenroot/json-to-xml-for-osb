package net.jsontoxmlbeans.inner;

import java.io.StringReader;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;

import argo.saj.SajParser;

public class JsonToXmlBeansConvertor {

	public JsonToXmlBeansConvertor() throws XmlException {
	}

	public XmlObject getXML(String json) throws XmlException {
		StringReader sr = new StringReader(json);
		
		JsonToXmlBeansListener listener = new JsonToXmlBeansListener();
		
		try {
			SajParser jsonParser = new SajParser();
			jsonParser.parse(sr, listener);
			
			sr.close();
			
			return listener.getXmlObject();
		} catch( Exception ex ) {
			throw new XmlException(ex);
		}
	}

}
