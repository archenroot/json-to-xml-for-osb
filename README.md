# Java library to convert JSON into XML for use in Oracle OSB

Oracle OSB doesn't have a native support for JSON. 

When a JSON request has to be modified, it first has to be converted into XML, then transformed using XQuery or XSLT, and then converted back into JSON again.

The internal representation of XML on OSB is Apache XmlBeans. It is thus most efficient to transform JSON into the XmlBeans data model. The code in this project does exactly that.

## In Scope

Parsing incoming JSON requests or responses into XmlBeans objects understood by OSB.

Generating JSON payload from XmlBeans objects built according to schema.

## Out of Scope

The code is **not** to convert any **arbitrary** XML into JSON. Only XMLs valid against the schema are converted into JSON. 

## Download

https://bitbucket.org/vladimirdyuzhev/json-to-xml-for-osb/downloads/osb.json.to.xml.jar

## Usage

### From JSON to XML

To convert into XML, call (do a Java callout) the following method:

```
#!java
net.jsontoxmlbeans.Convertor.toXML($body)
```

or, if the proxy is XML or SOAP based, the first child of the body which should be a text node containing the JSON payload: 

```
#!java
net.jsontoxmlbeans.Convertor.toXML(data($body))
```

The result is a XmlBeans object, i.e. you do not need to call fn-bea:inlinedXml() on the response.

For example, this JSON structure

```
[1,"foo",true,false,{"val":123},[5,6,7],null]
```

will be converted into this XML:

```
<json xmlns="http://json.to.xml.for.osb">
  <array>
    <number value="1"/>
    <string value="foo"/>
    <boolean value="true"/>
    <boolean value="false"/>
    <object>
      <field name="val">
        <number value="123"/>
      </field>
    </object>
    <array>
      <number value="5"/>
      <number value="6"/>
      <number value="7"/>
    </array>
    <null/>
  </array>
</json>
```

The complete schema is available in the sources as src/net/jsontoxmlbeans/osb.json.to.xml.xsd.

### From XML to JSON

To convert the updated XML back into JSON, do a Java callout to the following method:

```
#!java
net.jsontoxmlbeans.Convertor.toJSON($val)
```

The result is returned as string.

Please note that the XML must be valid against the schema. Though the validation is not performed during the toJSON() call, the non-valid 
XMLs will not be converted correctly.

### Special notes

#### Single vs Double Quotes

JSON field names and string values enclosed into single quotes are invalid. This library only recognizes double-quotes, as per standard (see http://json.org).

I.e. this is a valid syntax:

```
{ "name":"value" }
```

while this is not:

```
{ 'name':'value' }
```

## Addendum. Blog Posts About Making this Library

http://genericparallel.com/2014/08/osb-and-json-proxies-inspecting-modifying-the-payload/

http://genericparallel.com/2014/09/hiding-dependency-libraries-in-osb-callout-jar/

## License 

License is BSD, i.e. no restriction to use in commercial software:

Copyright (c) 2014, Vladimir Dyuzhev
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.