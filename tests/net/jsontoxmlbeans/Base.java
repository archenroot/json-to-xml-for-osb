package net.jsontoxmlbeans;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import junit.framework.TestCase;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class Base extends TestCase {
	protected String readFile(String file) throws FileNotFoundException, IOException {
		FileReader fr = new FileReader(file);
		StringBuilder sb = new StringBuilder();
		
		char buf[] = new char[4096];
		while( true ) {
			int read = fr.read(buf);
			if( read < 0 ) break;
			sb.append(buf,0,read);
		}
		fr.close();
		return sb.toString();
	}

	protected void assertXPath(String expected,String xpath,String xml) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(new InputSource(new StringReader(xml)));		
		
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xp = xPathfactory.newXPath();
		XPathExpression expr = xp.compile(xpath);		
		
		String res = expr.evaluate(doc);
		assertEquals(xpath,expected,res);
	}
	
	protected void assertSchema(String xml) throws SAXException, IOException {
		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

		URL schemaFile = new URL("file:src/net/jsontoxmlbeans/osb.json.to.xml.xsd");
		Schema schema = schemaFactory.newSchema(schemaFile);

		Source src = new StreamSource(new StringReader(xml));
		Validator validator = schema.newValidator();
		
		validator.validate((Source) src);
	}
}
