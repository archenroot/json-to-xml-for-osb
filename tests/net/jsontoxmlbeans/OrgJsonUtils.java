package net.jsontoxmlbeans;

import org.json.JSONObject;
import org.json.XML;

public class OrgJsonUtils {

    public static String toXml(String json) {
    	if( json == null ) return null;
    	
        JSONObject jo = new JSONObject(json);

        return XML.toString( jo );
    }

    public static String toJson(String xml) {
    	if( xml == null ) return null;
    	
        JSONObject jo = XML.toJSONObject(xml);

        return jo.toString();
    }
}
