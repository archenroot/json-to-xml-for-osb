package net.jsontoxmlbeans;

import org.apache.xmlbeans.XmlObject;

public class TestOrgJson extends Base {

	public void testBug_WrongDataTypes() throws Exception {
		String orig = "{\"bug\":\"777\",\"bug2\":\"true\"}";
		
		XmlObject xml = Convertor.toXML(orig);
		String s = xml.toString();
		
		// both values are strings, not number and boolean
		assertXPath("777","/json/object/field[@name='bug']/string/@value",s);
		assertXPath("true","/json/object/field[@name='bug2']/string/@value",s);
		
		String json = Convertor.toJSON(xml);
		assertEquals(orig,json);

		// and org.json fail to recognized the data types
		String xml2 = OrgJsonUtils.toXml(orig);
		String json2 = OrgJsonUtils.toJson(xml2);
		
		xml = Convertor.toXML(json2);
		s = xml.toString();
		assertXPath("777","/json/object/field[@name='bug']/number/@value",s);
		assertXPath("true","/json/object/field[@name='bug2']/boolean/@value",s);
	}
	
	public void testBug_NonWellFormedXML() throws Exception {
		String orig = "{\"hello world\":\"777\"}";
		
		XmlObject xml = Convertor.toXML(orig);
		String s = xml.toString();
		assertXPath("777","/json/object/field[@name='hello world']/string/@value",s);
		
		String json = Convertor.toJSON(xml);
		assertEquals(orig,json);

		// and org.json fail to recognized the data types
		String xml2 = OrgJsonUtils.toXml(orig);
		assertTrue(xml2.contains("<hello world>"));

		try {
			OrgJsonUtils.toJson(xml2);
			fail("shoulda have failed");
		} catch( Exception ex ) {
			// as expected, not a well-formed xml
		}
	}
}
