package net.jsontoxmlbeans;

import org.apache.xmlbeans.XmlObject;

/**
 * i7 results:
 * 
 * At 1000 cycles:
 * JSON to XML, elapsed time, ms: 26281
 * JSON to XML, one cycle, ms: 26
 * XML to JSON, elapsed time, ms: 35266
 * XML to JSON, one cycle, ms: 35
 *
 */
public class TestPerformance extends Base {

	private static int CYCLES = 1000;
	
	public void test165K_DirectlyToXML() throws Exception {
		String json = readFile("tests/165K.json");
		
		// warming up
		for( int i=0; i<CYCLES/10; i++ ) {
			Convertor.toXML(json);
		}

		long s = System.currentTimeMillis();
		for( int i=0; i<CYCLES; i++ ) {
			Convertor.toXML(json);
		}
		long e = System.currentTimeMillis();
		
		System.out.printf("Direct JSON to XML, elapsed time, ms: %d\n",e-s);
		System.out.printf("Direct JSON to XML, one cycle, ms: %d\n",(e-s)/CYCLES);
		
//		XmlObject xml = Convertor.toXML(json);
//
//		// warming up
//		for( int i=0; i<CYCLES/10; i++ ) {
//			Convertor.toJSON(xml);
//		}
//		
//		s = System.currentTimeMillis();
//		for( int i=0; i<CYCLES; i++ ) {
//			Convertor.toJSON(xml);
//		}
//		e = System.currentTimeMillis();
//		
//		System.out.printf("Direct XML to JSON, elapsed time, ms: %d\n",e-s);
//		System.out.printf("Direct XML to JSON, one cycle, ms: %d\n",(e-s)/CYCLES);
	}

	public void ZZZtest165K_ToXMLViaString() throws Exception {
		String json = readFile("tests/165K.json");
		
		// warming up
		for( int i=0; i<CYCLES/10; i++ ) {
			String xml = OrgJsonUtils.toXml(json);
			XmlObject.Factory.parse(xml);
		}

		long s = System.currentTimeMillis();
		for( int i=0; i<CYCLES; i++ ) {
			String xml = OrgJsonUtils.toXml(json);
			XmlObject.Factory.parse(xml);
		}
		long e = System.currentTimeMillis();
		
		System.out.printf("String JSON to XML, elapsed time, ms: %d\n",e-s);
		System.out.printf("String JSON to XML, one cycle, ms: %d\n",(e-s)/CYCLES);
		
		String xml = OrgJsonUtils.toXml(json);
		XmlObject obj = XmlObject.Factory.parse(xml);

		// warming up
		for( int i=0; i<CYCLES/10; i++ ) {
			String xml2 = obj.toString(); 
			OrgJsonUtils.toJson(xml2);
		}
		
		s = System.currentTimeMillis();
		for( int i=0; i<CYCLES; i++ ) {
			String xml2 = obj.toString();
			OrgJsonUtils.toJson(xml2);
		}
		e = System.currentTimeMillis();
		
		System.out.printf("String XML to JSON, elapsed time, ms: %d\n",e-s);
		System.out.printf("String XML to JSON, one cycle, ms: %d\n",(e-s)/CYCLES);
	}
}
